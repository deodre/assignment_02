
const FIRST_NAME = "Teodor";
const LAST_NAME = "Cervinski";
const GRUPA = "1075";

/**
 * Make the implementation here
 */
function initCaching() {
    let cache = {

        pageAccessCounter(section) {
            if(!section) {
                if(cache["home"]) {
                    cache["home"]++;
                }
                else {
                    cache["home"]=1;
                }
            }
            else {
                section=section.toLowerCase();
                if(cache[section]) {
                    cache[section]++;
                }
                else {
                    cache[section] = 1;
                }        
            }
        },

        getCache() {
            return cache;
        }
    }
    return cache;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

